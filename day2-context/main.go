package main

import (
	"net/http"

	"gitee.com/hlwqds/seven-days-golang/day2-context/gee"
)

func main() {
	r := gee.New()
	r.Use(gee.Logger(), gee.Recovery())
	r.GET("/", func(c *gee.Context) {
		c.String(http.StatusOK, "Hello Geektutu\n")
	})
	// index out of range for testing Recovery()
	r.GET("/panic", func(c *gee.Context) {
		names := []string{"geektutu"}
		c.String(http.StatusOK, names[100])
	})

	r.Run(":9999")
}
