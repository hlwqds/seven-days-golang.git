package main

import (
	"fmt"
	"net/http"

	"gitee.com/hlwqds/seven-days-golang/day1-http-base/base3/gee"
)

func main() {
	gee := gee.New()
	gee.GET("/", indexHandler)
	gee.GET("/hello", helloHandler)
	gee.Run(":9999")
}

// handler echoes r.URL.Path
func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "URL.Path = %q\n", r.URL.Path)
}

// handler echoes r.URL.Header
func helloHandler(w http.ResponseWriter, r *http.Request) {
	for k, v := range r.Header {
		fmt.Fprintf(w, "Header[%q] = %q\n", k, v)
	}
}
